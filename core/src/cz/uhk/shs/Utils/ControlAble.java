package cz.uhk.shs.Utils;

public interface ControlAble {
    public boolean tap(float x, float y, int count, int button);
}
