package cz.uhk.shs.Utils;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface RenderAble {
    public void render();
}
