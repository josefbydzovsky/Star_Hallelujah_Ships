package cz.uhk.shs;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;

import java.util.LinkedList;

import cz.uhk.shs.GameStages.Game;
import cz.uhk.shs.GameStages.GameOver;
import cz.uhk.shs.GameStages.GameStages;
import cz.uhk.shs.GameStages.GameWin;
import cz.uhk.shs.GameStages.OpeningMenu;
import cz.uhk.shs.Model.Player;

public class StarHallelujahShips extends ApplicationAdapter implements GestureDetector.GestureListener {
	public static float onePercentWidth, onePercentHeight;

	SpriteBatch batch;
	BitmapFont font;
	GestureDetector gestureDetector;

    public static GameStages gameStage;
	OpeningMenu openingMenu;
    public static Game game;
	GameOver gameOver;
	GameWin gameWin;

    public static Player player;
	public static LinkedList<Player> npcs = new LinkedList<Player>();

	@Override
	public void create () {
		batch = new SpriteBatch();
		font = new BitmapFont();
		font.setColor(Color.RED);
		gestureDetector = new GestureDetector(this);
		Gdx.input.setInputProcessor(gestureDetector);
		onePercentWidth = Gdx.graphics.getWidth()/100;
		onePercentHeight = Gdx.graphics.getHeight()/100;

		gameStage = GameStages.OPENING_MENU;
		openingMenu = new OpeningMenu(batch);
	}

	@Override
	public void render () {
		switch (gameStage){
			case OPENING_MENU:
				openingMenu.render();
				break;
			case GAME:
				if(game != null){
                    game.render();
                }else{
                    game = new Game(batch);
                }
                break;
			case GAMEOVER:
				if(gameOver == null) gameOver = new GameOver(batch);
				gameOver.render();
				break;
			case GAMEWIN:
				if(gameWin == null) gameWin = new GameWin(batch);
				gameWin.render();
				break;
			default:
                Gdx.gl.glClearColor(0, 0, 0, 1);
                Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
                batch.begin();
                batch.setTransformMatrix(new Matrix4().setToScaling(4.0f,4.0f,0.0f).translate(onePercentWidth*10.0f, onePercentHeight*10.0f, 0.0f));
                font.draw(batch, "Hups, něco se pokazilo",0.0f,0.0f);
                batch.end();
                break;
		}
	}

    //cotrol
    @Override
    public boolean tap(float x, float y, int count, int button) {
        switch (gameStage){
            case OPENING_MENU:
                return openingMenu.tap(x, y, count, button);
            case GAME:
                return game.tap(x, y, count, button);
			case GAMEOVER:
				return gameOver.tap(x, y, count, button);
			case GAMEWIN:
				return gameWin.tap(x, y, count, button);
            default:
                return false;
        }
    }

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		return false;
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		return false;
	}

	@Override
	public void pinchStop() {
		int i = 0;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		return false;
	}

	//dispose
	@Override
	public void dispose () {
		batch.dispose();
		font.dispose();
	}
}
