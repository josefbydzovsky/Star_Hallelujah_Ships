package cz.uhk.shs.GameStages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.util.LinkedList;

import cz.uhk.shs.Model.Player;
import cz.uhk.shs.StarHallelujahShips;
import cz.uhk.shs.Utils.ControlAble;
import cz.uhk.shs.Utils.RenderAble;

public class GameOver implements RenderAble, ControlAble {
    private Texture backGround;
    SpriteBatch batch;

    Stage stage;

    public GameOver(SpriteBatch batch){
        backGround = new Texture(Gdx.files.internal("GameOver\\GameOverBackGround.jpg"));
        this.batch = batch;
        Gdx.gl.glClearColor(255, 255, 255, 255);

        this.stage = new Stage(new ScreenViewport());
        Image napisKonecHry = new Image(new Texture(Gdx.files.internal("GameOver\\napisGameOver.png")));
        napisKonecHry.setX(StarHallelujahShips.onePercentWidth*50.0f);
        napisKonecHry.setY(StarHallelujahShips.onePercentHeight*50.0f);
        napisKonecHry.setWidth(StarHallelujahShips.onePercentWidth*20.0f);
        napisKonecHry.setHeight(StarHallelujahShips.onePercentHeight*20.0f);
        napisKonecHry.setName("napisKonecHry");
        stage.addActor(napisKonecHry);

        Image napisHlavníMenu = new Image(new Texture(Gdx.files.internal("GameOver\\napisMainMenu.png")));
        napisHlavníMenu.setX(StarHallelujahShips.onePercentWidth*2.0f);
        napisHlavníMenu.setY(StarHallelujahShips.onePercentHeight*2.0f);
        napisHlavníMenu.setWidth(StarHallelujahShips.onePercentWidth*20.0f);
        napisHlavníMenu.setHeight(StarHallelujahShips.onePercentHeight*10.0f);
        napisHlavníMenu.setName("napisHlavniMenu");
        stage.addActor(napisHlavníMenu);
    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        batch.draw(backGround, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        batch.end();

        stage.draw();
    }

    @Override
    public boolean tap(float x, float y, int count, int button){
        Vector2 touched = stage.screenToStageCoordinates(new Vector2(x, y));
        Actor actor = stage.hit(touched.x, touched.y, true);
        if(actor != null){
            if(actor.getName().equals("napisHlavniMenu")){
                StarHallelujahShips.gameStage = GameStages.OPENING_MENU;
                StarHallelujahShips.game = null;
                StarHallelujahShips.player = null;
                StarHallelujahShips.npcs = new LinkedList<Player>();
            }
        }
        return true;
    }
}
