package cz.uhk.shs.GameStages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.util.LinkedList;

import cz.uhk.shs.Model.Fleet;
import cz.uhk.shs.Model.Planet;
import cz.uhk.shs.Model.PlanetTypes;
import cz.uhk.shs.Model.Player;
import cz.uhk.shs.Model.Ship;
import cz.uhk.shs.StarHallelujahShips;
import cz.uhk.shs.Utils.ControlAble;
import cz.uhk.shs.Utils.RenderAble;

public class Game implements RenderAble, ControlAble {
    private Texture backGround;
    private Texture UI, ship, shipSelected;
    SpriteBatch batch;
    BitmapFont font;
    public LinkedList<Planet> Planets = new LinkedList<Planet>();
    public LinkedList<Fleet> Fleets = new LinkedList<Fleet>();
    Actor selectedPlanet, selectedFleet;
    Stage stage;

    public Game(SpriteBatch batch){
        this.batch = batch;
        this.font = new BitmapFont();
        this.font.getData().setScale(10);
        this.font.setColor(Color.BLUE);
        this.backGround = new Texture(Gdx.files.internal("Game\\GameBackGround.jpg"));
        this.UI = new Texture(Gdx.files.internal("Game\\UI\\UI.png"));
        this.ship = new Texture(Gdx.files.internal("Game\\UI\\ship.png"));
        this.shipSelected = new Texture(Gdx.files.internal("Game\\UI\\shipSelected.png"));
        Gdx.gl.glClearColor(255, 255, 255, 255);

        initGame();
        this.stage = initStage();
    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if(StarHallelujahShips.player.getOwnedPlanets().size() > 0 && StarHallelujahShips.player.getOwnedPlanets().size() != Planets.size()){
            batch.begin();
            batch.draw(backGround, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            batch.end();

            stage.act();
            stage.draw();

            batch.begin();
            batch.draw(UI, 0, 0, StarHallelujahShips.onePercentWidth*50.0f,  StarHallelujahShips.onePercentHeight*25.0f);
            if(selectedPlanet != null) {
                selectedFleet.draw(batch, 0.0f);
                font.draw(batch, String.valueOf(((Planet)selectedPlanet).ships.size()), StarHallelujahShips.onePercentWidth*25, StarHallelujahShips.onePercentHeight*20);
            }
            batch.end();

            for(int i = 0; i < StarHallelujahShips.npcs.size(); i++){
                StarHallelujahShips.npcs.get(i).act();
            }
        }
        else{
            if(StarHallelujahShips.player.getOwnedPlanets().size() == 0){
                StarHallelujahShips.gameStage = GameStages.GAMEOVER;
            }
            else{
                StarHallelujahShips.gameStage = GameStages.GAMEWIN;
            }
        }
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        Vector2 touched = stage.screenToStageCoordinates(new Vector2(x, y));
        Actor selected = stage.hit(touched.x, touched.y, true);
        if(selected != null){
            if(selected.getClass().equals(Planet.class)) {
                if(((Fleet)selectedFleet).getDrawStatus() == 2){
                    if(selected != selectedPlanet && ((Planet)selectedPlanet).ships.size() > 0){
                        Fleets.add(new Fleet(((Planet)selectedPlanet).ships, ((Planet)selectedPlanet), ((Planet)selected) ));
                        ((Planet)selectedPlanet).ships = new LinkedList<Ship>();
                        stage.addActor(Fleets.getLast());
                        if(selectedPlanet != null){
                            ((Planet)selectedPlanet).unSelect();
                            selectedPlanet = null;
                        }
                        ((Fleet)selectedFleet).setDrawStatus(0);
                    }
                }
                else{
                    if(((Planet)selected).owner.getName().equals("Player")){
                        if(selectedPlanet != null){
                            ((Planet)selectedPlanet).unSelect();
                        }
                        selectedPlanet = selected;
                        ((Planet) selected).select();
                        ((Fleet)selectedFleet).setDrawStatus(1);
                    }
                }
            }
            else if(selected == selectedFleet){
                ((Fleet)selectedFleet).setDrawStatus(2);
            }
        }
        else{
            if(selectedPlanet != null){
                ((Planet)selectedPlanet).unSelect();
                selectedPlanet = null;
            }
            ((Fleet)selectedFleet).setDrawStatus(0);
        }
        return true;
    }

    private void initGame(){
        selectedFleet = new Fleet();

        StarHallelujahShips.player = new Player("Player");
        StarHallelujahShips.npcs.add(new Player("NPC1"));
        StarHallelujahShips.npcs.add(new Player("NPC2"));

        Planets.add(new Planet(PlanetTypes.VENUSE, new Vector2(StarHallelujahShips.onePercentWidth * 25.0f, StarHallelujahShips.onePercentHeight * 25.0f), new Vector2(StarHallelujahShips.onePercentWidth * 10.0f, StarHallelujahShips.onePercentHeight * 10.0f), StarHallelujahShips.player));
        StarHallelujahShips.player.getOwnedPlanets().add(Planets.getLast());

        Planets.add(new Planet(PlanetTypes.URAN, new Vector2(StarHallelujahShips.onePercentWidth * 35.0f, StarHallelujahShips.onePercentHeight * 50.0f), new Vector2(StarHallelujahShips.onePercentWidth * 18.0f, StarHallelujahShips.onePercentHeight * 18.0f), StarHallelujahShips.player));
        StarHallelujahShips.player.getOwnedPlanets().add(Planets.getLast());

        Planets.add(new Planet(PlanetTypes.SATURN, new Vector2(StarHallelujahShips.onePercentWidth * 5.0f, StarHallelujahShips.onePercentHeight * 80.0f) ,new Vector2(StarHallelujahShips.onePercentWidth * 25.0f, StarHallelujahShips.onePercentHeight * 25.0f), StarHallelujahShips.npcs.get(0)));
        StarHallelujahShips.npcs.get(0).getOwnedPlanets().add(Planets.getLast());

        Planets.add(new Planet(PlanetTypes.JUPITER, new Vector2(StarHallelujahShips.onePercentWidth * 80.0f, StarHallelujahShips.onePercentHeight * 40.0f) ,new Vector2(StarHallelujahShips.onePercentWidth * 15.0f, StarHallelujahShips.onePercentHeight * 15.0f), StarHallelujahShips.npcs.get(0)));
        StarHallelujahShips.npcs.get(0).getOwnedPlanets().add(Planets.getLast());

        Planets.add(new Planet(PlanetTypes.PLUTO, new Vector2(StarHallelujahShips.onePercentWidth * 5.0f, StarHallelujahShips.onePercentHeight * 50.0f) ,new Vector2(StarHallelujahShips.onePercentWidth * 5.0f, StarHallelujahShips.onePercentHeight * 5.0f), StarHallelujahShips.npcs.get(0)));
        StarHallelujahShips.npcs.get(0).getOwnedPlanets().add(Planets.getLast());

        Planets.add(new Planet(PlanetTypes.MERKUR, new Vector2(StarHallelujahShips.onePercentWidth * 55.0f, StarHallelujahShips.onePercentHeight * 80.0f) ,new Vector2(StarHallelujahShips.onePercentWidth * 8.0f, StarHallelujahShips.onePercentHeight * 8.0f), StarHallelujahShips.npcs.get(1)));
        StarHallelujahShips.npcs.get(1).getOwnedPlanets().add(Planets.getLast());

        Planets.add(new Planet(PlanetTypes.MARS, new Vector2(StarHallelujahShips.onePercentWidth * 75.0f, StarHallelujahShips.onePercentHeight * 10.0f) ,new Vector2(StarHallelujahShips.onePercentWidth * 8.0f, StarHallelujahShips.onePercentHeight * 8.0f), StarHallelujahShips.npcs.get(1)));
        StarHallelujahShips.npcs.get(1).getOwnedPlanets().add(Planets.getLast());

        Planets.add(new Planet(PlanetTypes.NEPTUN, new Vector2(StarHallelujahShips.onePercentWidth * 85.0f, StarHallelujahShips.onePercentHeight * 85.0f) ,new Vector2(StarHallelujahShips.onePercentWidth * 8.0f, StarHallelujahShips.onePercentHeight * 8.0f), StarHallelujahShips.npcs.get(1)));
        StarHallelujahShips.npcs.get(1).getOwnedPlanets().add(Planets.getLast());

    }

    private Stage initStage(){
        Stage stage = new Stage(new ScreenViewport());
        for(Planet planet : Planets){
            stage.addActor(planet);
        }
        stage.addActor(selectedFleet);
        return stage;
    }

    public void removeFleet(Fleet fleet){
        Fleets.remove(fleet);
        stage.getActors().removeValue(fleet, false);
    }

    public void addFleet(Fleet fleet){
        Fleets.add(fleet);
        stage.addActor(fleet);
    }
}
