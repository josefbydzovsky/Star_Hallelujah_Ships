package cz.uhk.shs.GameStages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import cz.uhk.shs.StarHallelujahShips;
import cz.uhk.shs.Utils.ControlAble;
import cz.uhk.shs.Utils.RenderAble;

public class OpeningMenu implements RenderAble, ControlAble {
    private Texture backGround;
    SpriteBatch batch;

    Stage stage;

    public OpeningMenu(SpriteBatch batch){
        backGround = new Texture(Gdx.files.internal("OpeningMenu\\OpeningMenuBackGround.jpg"));
        this.batch = batch;
        Gdx.gl.glClearColor(255, 255, 255, 255);

        this.stage = new Stage(new ScreenViewport());
        Image napisNovaHra = new Image(new Texture(Gdx.files.internal("OpeningMenu\\napisNovaHra.png")));
        napisNovaHra.setX(StarHallelujahShips.onePercentWidth*50.0f);
        napisNovaHra.setY(StarHallelujahShips.onePercentHeight*50.0f);
        napisNovaHra.setWidth(StarHallelujahShips.onePercentWidth*20.0f);
        napisNovaHra.setHeight(StarHallelujahShips.onePercentHeight*20.0f);
        napisNovaHra.setName("napisNovaHra");
        stage.addActor(napisNovaHra);
    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        batch.draw(backGround, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        batch.end();

        stage.draw();
    }

    @Override
    public boolean tap(float x, float y, int count, int button){
        Vector2 touched = stage.screenToStageCoordinates(new Vector2(x, y));
        Actor actor = stage.hit(touched.x, touched.y, true);
        if(actor != null && actor.getName().equals("napisNovaHra")){
            StarHallelujahShips.gameStage = GameStages.GAME;
        }
        return true;
    }
}
