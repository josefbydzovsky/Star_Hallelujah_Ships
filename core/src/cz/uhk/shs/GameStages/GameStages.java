package cz.uhk.shs.GameStages;

public enum GameStages {
    OPENING_MENU,
    GAME,
    GAMEOVER,
    GAMEWIN;
}
