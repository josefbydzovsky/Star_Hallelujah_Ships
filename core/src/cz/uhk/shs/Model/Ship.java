package cz.uhk.shs.Model;

import com.badlogic.gdx.scenes.scene2d.Actor;

public class Ship extends Actor {
    int armor, maxArmor;
    int shield, maxShield; float oneShieldPointRegenerationInSeconds, shieldRegenerationRecharge;
    boolean reloaded = false, destroied = false;

    int laser; float laserReloadTimeInSeconds, laserReload;

    public Ship(ShipTypes type) {
        switch (type){
            case BASIC:
                this.armor = 10;
                this.maxArmor = 10;
                this.shield = 10;
                this.maxShield = 10;
                this.oneShieldPointRegenerationInSeconds = 2.0f;
                this.shieldRegenerationRecharge = 0;
                this.laser = 4;
                this.laserReloadTimeInSeconds = 2;
                this.laserReload = 0;
                break;
            default:
                this.armor = -1;
                this.maxArmor = -1;
                this.shield = -1;
                this.maxShield = -1;
                this.oneShieldPointRegenerationInSeconds = -1.0f;
                this.shieldRegenerationRecharge = -1.0f;
                this.laser = -1;
                this.laserReloadTimeInSeconds = -1.0f;
                this.laserReload = -1.0f;
                break;
        }
    }

    public void takeHit(int dmg){
        if(shield >= dmg){
            shield -= dmg;
            return;
        }
        else if(shield < dmg && shield > 0){
            dmg -= shield;
            shield = 0;
            armor -= dmg;
        }
        else{
            armor -= dmg;
        }
        if(armor <= 0){
            destroied = true;
        }
    }

    public int shoot(){
        if(laserReload == laserReloadTimeInSeconds) {
            laserReload = 0.0f;
            return laser;
        }
        else{
            return 0;
        }
    }

    private void regenerateShield(float delta){
        if(shield == maxShield){
            return;
        }
        shieldRegenerationRecharge += delta/oneShieldPointRegenerationInSeconds;

        if(shieldRegenerationRecharge >= oneShieldPointRegenerationInSeconds){
            shield++;
            shieldRegenerationRecharge = 0;
        }
    }

    private void reloadLaser(float delta){
        if(laserReload == laserReloadTimeInSeconds){
            return;
        }
        laserReload += delta/laserReloadTimeInSeconds; // 1/laserReloadTimeInSeconds * delta
        if(laserReload >= laserReloadTimeInSeconds){
            laserReload = laserReloadTimeInSeconds;
            reloaded = true;
        }
    }

    @Override
    public void act(float delta) {
        regenerateShield(delta);
        reloadLaser(delta);
    }
}
