package cz.uhk.shs.Model;

import java.util.LinkedList;
import java.util.Random;

import cz.uhk.shs.StarHallelujahShips;

public class Player {
    String name;
    LinkedList<Planet> ownedPlanets = new LinkedList<Planet>();

    public Player(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setOwnedPlanets(LinkedList<Planet> ownedPlanets) {
        this.ownedPlanets = ownedPlanets;
    }

    public LinkedList<Planet> getOwnedPlanets() {
        return ownedPlanets;
    }

    public void act(){
        if(name.equals("Player")) return;
        Random r = new Random();
        if(r.nextInt(200) == 100){
            int planetIndex = -1;
            try{
                planetIndex = r.nextInt(ownedPlanets.size());
            }catch(Exception ex) {
                return;
            }
            if(ownedPlanets.get(planetIndex).ships.size() > 0){
                Planet targetPlanet = StarHallelujahShips.game.Planets.get(r.nextInt(StarHallelujahShips.game.Planets.size()));
                StarHallelujahShips.game.addFleet(new Fleet(ownedPlanets.get(planetIndex).ships, ownedPlanets.get(planetIndex), targetPlanet));
                ownedPlanets.get(planetIndex).ships = new LinkedList<Ship>();
            }
        }
    }
}
