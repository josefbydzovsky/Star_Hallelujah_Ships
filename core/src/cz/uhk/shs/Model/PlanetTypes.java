package cz.uhk.shs.Model;

public enum PlanetTypes {
    VENUSE,
    SATURN,
    MERKUR,
    JUPITER,
    NEPTUN,
    MARS,
    URAN,
    PLUTO;

    @Override
    public String toString() {
        switch (this){
            case VENUSE:
                return "Venuše";
            case SATURN:
                return "Saturn";
            case MERKUR:
                return "Merkur";
            case JUPITER:
                return "Jupiter";
            case NEPTUN:
                return "Neptun";
            case MARS:
                return "Mars";
            case URAN:
                return "Uran";
            case PLUTO:
                return "Pluto";
            default:
                return "Error";
        }
    }
}
