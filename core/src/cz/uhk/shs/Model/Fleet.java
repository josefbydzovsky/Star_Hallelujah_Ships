package cz.uhk.shs.Model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g3d.particles.influencers.ColorInfluencer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

import java.util.LinkedList;
import java.util.Random;

import cz.uhk.shs.StarHallelujahShips;

public class Fleet extends Actor{
    final float size = 5.0f;
    LinkedList<Ship> ships;
    Texture img, imgSelected;
    TextureRegion imgRegion;
    Vector2 moveVector;
    Planet begin, target;
    Vector2 vecBegin, vecTarget;
    Vector2 centerForRotation;
    float onWay, travelDistance, speed, angle;
    int drawStatus = 0; //0 - nevykresluju, 1 - vykresluju, 2 - vykresluju selected

    public Fleet(){
        this.img = new Texture(Gdx.files.internal("Game\\UI\\ship.png"));
        this.imgSelected = new Texture(Gdx.files.internal("Game\\UI\\shipSelected.png"));

        this.ships = null;
        this.begin = null;
        this.target = null;
        this.vecBegin = null;
        this.vecTarget = null;
        this.moveVector = null;
        this.centerForRotation = null;
        this.travelDistance = 1.0f;
        this.angle = 0.0f;
        this.onWay = 0.0f;
        this.speed = 0.0f;
        this.drawStatus = 0;
        setBounds(StarHallelujahShips.onePercentWidth, StarHallelujahShips.onePercentHeight*3, StarHallelujahShips.onePercentWidth*20, StarHallelujahShips.onePercentHeight*20);
    }

    public Fleet(LinkedList<Ship> ships, Planet begin, Planet target){
        this.ships = ships;
        this.begin = begin;
        this.target = target;
        this.vecBegin = new Vector2(begin.position.x + begin.size.x/2 - StarHallelujahShips.onePercentWidth*size/2, begin.position.y + begin.size.y/2 - StarHallelujahShips.onePercentHeight*size/2);
        this.vecTarget = new Vector2(target.position.x + target.size.x/2 - StarHallelujahShips.onePercentWidth*size/2, target.position.y + target.size.y/2 - StarHallelujahShips.onePercentHeight*size/2);;
        float yLength = Math.abs(vecBegin.y - vecTarget.y);
        float xLength = Math.abs(vecBegin.x - vecTarget.x);
        this.travelDistance = (float)Math.sqrt(yLength*yLength + xLength*xLength);
        this.moveVector = new Vector2((vecTarget.x - vecBegin.x)/travelDistance, (vecTarget.y - vecBegin.y)/travelDistance);
        this.centerForRotation = new Vector2( StarHallelujahShips.onePercentWidth*size/2, StarHallelujahShips.onePercentHeight*size/2);

        if(moveVector.x >= 0 && moveVector.y >= 0 ){
            this.angle = -(float)Math.toDegrees(Math.atan(xLength / yLength));
        }
        else if(moveVector.x < 0 && moveVector.y >= 0 ){
            this.angle = (float)Math.toDegrees(Math.atan(xLength / yLength));
        }
        else if(moveVector.x < 0 && moveVector.y < 0 ){
            this.angle = 180.0f - (float)Math.toDegrees(Math.atan(xLength / yLength));
        }
        else{ //moveVector.x >= 0 && moveVector.y < 0
            this.angle = 180.0f + (float)Math.toDegrees(Math.atan(xLength / yLength));
        }
        changeTexture();

        this.onWay = 0.0f;
        this.speed = (float)Math.sqrt(StarHallelujahShips.onePercentWidth*StarHallelujahShips.onePercentHeight) *5.0f;
        this.drawStatus =1;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        if(this.ships == null) {
            switch (this.drawStatus){
                case 1:
                    batch.draw(img, StarHallelujahShips.onePercentWidth, StarHallelujahShips.onePercentHeight*3, StarHallelujahShips.onePercentWidth*20, StarHallelujahShips.onePercentHeight*20);
                    break;
                case 2:
                    batch.draw(imgSelected, StarHallelujahShips.onePercentWidth, StarHallelujahShips.onePercentHeight*3, StarHallelujahShips.onePercentWidth*20, StarHallelujahShips.onePercentHeight*20);
                    break;
            }
        }
        else{
            Vector2 position = new Vector2(vecBegin.x + moveVector.x * onWay, vecBegin.y + moveVector.y * onWay);
            batch.draw(imgRegion, position.x, position.y, centerForRotation.x, centerForRotation.y, StarHallelujahShips.onePercentWidth*size, StarHallelujahShips.onePercentHeight*size,1.0f,1.0f,angle);
        }
    }

    @Override
    public void act(float delta) {
        this.onWay += this.speed * delta;
        if(this.onWay >= travelDistance){
            this.speed = 0.0f;
            this.onWay = travelDistance;
            if(begin.owner.equals(target.owner)){
                target.ships.addAll(this.ships);
                StarHallelujahShips.game.removeFleet(this);
            }
            else{
                Random r = new Random();
                delta = 2.0f;
                while(this.ships.size() > 0 && target.ships.size() > 0){
                    for(int i = 0; i < this.ships.size(); i++){
                        Ship sourceOFFire = this.ships.get(i);
                        sourceOFFire.act(delta);
                        if(sourceOFFire.reloaded){
                            Ship targetOfFire = target.ships.get(r.nextInt(target.ships.size()));
                            targetOfFire.takeHit(sourceOFFire.shoot());
                            if(targetOfFire.destroied){
                                target.ships.remove(targetOfFire);
                                if(target.ships.size() == 0){
                                    break;
                                }
                            }
                        }
                    }
                    for(int i = 0; i < target.ships.size(); i++){
                        Ship sourceOFFire = target.ships.get(i);
                        sourceOFFire.act(delta);
                        if(sourceOFFire.reloaded){
                            Ship targetOfFire = this.ships.get(r.nextInt(this.ships.size()));
                            targetOfFire.takeHit(sourceOFFire.shoot());
                            if(targetOfFire.destroied){
                                this.ships.remove(targetOfFire);
                                if(this.ships.size() == 0){
                                    break;
                                }
                            }
                        }
                    }
                }
                if(this.ships.size() == 0){
                    StarHallelujahShips.game.removeFleet(this);
                }
                else{
                    target.ships.addAll(this.ships);
                    StarHallelujahShips.game.removeFleet(this);
                    target.owner.getOwnedPlanets().remove(target);
                    target.owner = begin.owner;
                    target.changeTexture();
                    begin.owner.getOwnedPlanets().add(target);
                }
            }
        }
    }

    public  void setDrawStatus(int drawStatus){
        switch (drawStatus){
            case 1:
            case 2:
                this.drawStatus = drawStatus;
                setBounds(StarHallelujahShips.onePercentWidth, StarHallelujahShips.onePercentHeight*3, StarHallelujahShips.onePercentWidth*20, StarHallelujahShips.onePercentHeight*20);
                break;
            case 0:
            default:
                this.drawStatus = 0;
                setBounds(0, 0, 0, 0);
                break;

        }
    }

    public int getDrawStatus(){
        return drawStatus;
    }

    public void changeTexture(){
        this.img = new Texture(Gdx.files.internal("Game\\UI\\ship"+ begin.owner.getName() +".png"));
        this.imgRegion = new TextureRegion(img);
    }
}
