package cz.uhk.shs.Model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

import java.util.LinkedList;

import cz.uhk.shs.StarHallelujahShips;

public class Planet extends Actor {
    Texture img;
    public Vector2 position;
    Vector2 size;

    PlanetTypes type;
    public Player owner;
    public LinkedList<Ship> ships = new LinkedList<Ship>();
    float shipBuildingSpeedOfOneShipInSeconds, shipBuildingInProgress;

    public Planet(PlanetTypes type, Vector2 position, Vector2 size, Player owner) {
        this.type = type;
        this.position = position;
        this.size = size;
        this.setName(type.toString());
        this.owner = owner;
        setBounds(position.x,position.y,size.x,size.y);
        if(owner.getName().equals("Player")){
            this.img = new Texture(Gdx.files.internal("Planets\\planet"+ type.toString() +".png"));
        }
        else{
            this.img = new Texture(Gdx.files.internal("Planets\\planet"+ type.toString() + owner.getName() +".png"));
        }
        initPlanetSpecifications(type);
    }

    private void initPlanetSpecifications(PlanetTypes type) {

        this.shipBuildingInProgress = 0.0f;

        switch (type){
            case SATURN:
                this.shipBuildingSpeedOfOneShipInSeconds = 3.0f;
                return;
            case VENUSE:
                this.shipBuildingSpeedOfOneShipInSeconds = 2.5f;
                return;
            case MERKUR:
                this.shipBuildingSpeedOfOneShipInSeconds = 3.5f;
                return;
            case JUPITER:
                this.shipBuildingSpeedOfOneShipInSeconds = 2.7f;
                return;
            case NEPTUN:
                this.shipBuildingSpeedOfOneShipInSeconds = 2.2f;
                return;
            case MARS:
                this.shipBuildingSpeedOfOneShipInSeconds = 3.7f;
                return;
            case URAN:
                this.shipBuildingSpeedOfOneShipInSeconds = 3.2f;
                return;
            case PLUTO:
                this.shipBuildingSpeedOfOneShipInSeconds = 2.5f;
                return;
            default:
                return;
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(img, position.x, position.y, size.x, size.y);
    }

    @Override
    public void act(float delta) {
        shipBuildingInProgress += delta/shipBuildingSpeedOfOneShipInSeconds;
        if(shipBuildingInProgress >= shipBuildingSpeedOfOneShipInSeconds){
            shipBuildingInProgress = 0.0f;
            ships.add(new Ship(ShipTypes.BASIC));
        }
    }

    public void setOwner(Player owner) {
        this.owner = owner;
    }

    public void unSelect() {
        img = new Texture(Gdx.files.internal("Planets\\planet"+ type.toString() +".png"));
    }

    public void select() {
        img = new Texture(Gdx.files.internal("Planets\\planet"+ type.toString() +"Selected.png"));
    }

    public void changeTexture(){
        if(owner.getName().equals("Player")){
            this.img = new Texture(Gdx.files.internal("Planets\\planet"+ type.toString() +".png"));
        }
        else{
            this.img = new Texture(Gdx.files.internal("Planets\\planet"+ type.toString() + owner.getName() +".png"));
        }
        for(int i = 0; i < StarHallelujahShips.game.Fleets.size(); i++){
            if(StarHallelujahShips.game.Fleets.get(i).begin.equals(this)){
                StarHallelujahShips.game.Fleets.get(i).changeTexture();
            }
        }
    }
}
