package cz.uhk.shs.Model;

public enum ShipTypes {
    BASIC;

    @Override
    public String toString() {
        switch (this){
            case BASIC:
                return "shipBasic";
            default:
                return "shipERROR";
        }
    }
}
